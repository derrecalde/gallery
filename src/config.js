import * as firebase from "firebase";

// Add the Firebase services that you want to use
import "firebase/firestore";

// All credentials inside Firebase account/my project/settings
 let config = {
  apiKey: "*",
  authDomain: "*",
  databaseURL: "*",
  projectId: "*",
  storageBucket: "*"    
};
let app = firebase.initializeApp(config);

const db = app.firestore()

export {db, config, firebase}
// export const db = app.database()
